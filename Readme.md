# ETDWalker (Working Title) #

**Description:**

A *bot* to Download much everything from [ETD Universitas Gadjah Mada](http://etd.repository.ugm.ac.id/). This software automatically downloads all available research report pdfs (which is split into title page, abstract, introduction, conclusion, and bibliography) for given list in csv format. The csv itself should had at least 5 column:

* author
* title
* type
* city of publishing
* year
* publisher

**Requirement:**

* [Java Development Kit 8](www.oracle.com/technetwork/java/javase/downloads/)
* [Apache HTTP Components 4.5.1](http://mirror.wanxp.id/apache//httpcomponents/httpclient/binary/httpcomponents-client-4.5.1-bin.zip)
* [jsoup 1.8.3](http://jsoup.org/download)
* [Apache Commons CSV 1.2](https://commons.apache.org/proper/commons-csv)

**Future Works:**

* Externalise model, model-building rule, and rule of transversing model
* Externalise CSV reading rule
* Custorm CSV format or structure
* Multithreading
* GUI and or CLI

**Disclaimer:**

Any CSV that stored within this repository is a personal property of UGM and should not misused in anyway to harm the owner.
