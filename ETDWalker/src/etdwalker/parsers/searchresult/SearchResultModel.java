/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.searchresult;

import java.util.LinkedList;
/**
 *
 * @author Tezla
 */
public class SearchResultModel {
    //stored 'just above' top right corner of the table
    private int itemFrom;
    private int itemTo;
    private int itemMax;
    //stored in table
    private LinkedList<SearchItem> items;

    /**
     * @return the itemFrom
     */
    int getItemFrom() {
        return itemFrom;
    }

    /**
     * @param itemFrom the itemFrom to set
     */
    void setItemFrom(int itemFrom) {
        this.itemFrom = itemFrom;
    }

    /**
     * @return the itemTo
     */
    int getItemTo() {
        return itemTo;
    }

    /**
     * @param itemTo the itemTo to set
     */
    void setItemTo(int itemTo) {
        this.itemTo = itemTo;
    }

    /**
     * @return the itemMax
     */
    public int getItemMax() {
        return itemMax;
    }

    /**
     * @param itemMax the itemMax to set
     */
    void setItemMax(int itemMax) {
        this.itemMax = itemMax;
    }

    /**
     * @return the items
     */
    public LinkedList<SearchItem> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(LinkedList<SearchItem> items) {
        this.items = items;
    }
    
    @Override
    public String toString(){
        String out = "{" + "\n"
                + "from: " + this.itemFrom + "\n"
                + "to: " + this.itemTo + "\n"
                + "max: " + this.itemMax + "\n"
                + "result: " + this.items + "\n"
                + "}";
        return out;
    }
    
}
