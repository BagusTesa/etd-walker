/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.searchresult;

/**
 *
 * @author Tezla
 */
public class SearchItem {
    private String title;
    private String url;
    
    public SearchItem(String title, String url){
        this.title = title;
        this.url = url;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    void setUrl(String url) {
        this.url = url;
    }
    
    @Override
    public String toString(){
        return this.title + "[" + this.url + "]";
    }
}
