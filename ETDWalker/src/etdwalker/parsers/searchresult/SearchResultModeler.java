/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.searchresult;

import java.util.LinkedList;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.parser.Parser;
/**
 * Convert HTML to SearchResultModel with SearchItem inside.
 * 
 * 
 * @author Tezla
 */
public class SearchResultModeler {
    public static SearchResultModel modelize(String htmlText, String originalURL){
        SearchResultModel out = new SearchResultModel();
        Document document = Parser.parse(htmlText, originalURL);
        Element body = document.body();
        
        //multistage safety net
        if(body != null){
            Element resultTable = body.getElementsByClass("table-common").first();
            if(resultTable != null){
                Elements alink = resultTable.getElementsByTag("a");
                
                LinkedList<SearchItem> srl = new LinkedList<>();
                for(Element atrack: alink){
                    if(atrack.attr("href").contains("book_detail")){
                        SearchItem sri = new SearchItem(atrack.text(), atrack.attr("href"));
                        srl.add(sri);
                    }
                }
                out.setItems(srl);
                
                Element pageNav = body.getElementsByClass("pageNav").first();
                if(pageNav != null){
                    //it had page navigation, it's >1 page
                    Element pageInfo = pageNav.select(".pageInfo").first();
                    Elements pageInfoNums = pageInfo.getElementsByTag("b");
                    if(pageInfoNums.size() == 3){
                        out.setItemFrom(Integer.parseInt(pageInfoNums.get(0).text()));
                        out.setItemTo(Integer.parseInt(pageInfoNums.get(1).text()));
                        out.setItemMax(Integer.parseInt(pageInfoNums.get(2).text()));
                    }
                }
                else{
                    //no page navigation, it can either empty or single page
                    if(out.getItems() != null){
                        //search result just a single page
                        out.setItemFrom(1);
                        out.setItemMax(out.getItems().size());
                        out.setItemTo(out.getItems().size());
                    }
                    else{
                        //no result found perhaps
                        out.setItemFrom(0);
                        out.setItemMax(0);
                        out.setItemTo(0);
                    }
                }
            }
            return out;
        }
        
        return null;
    }
}
