/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.item;

import java.util.LinkedList;
/**
 *
 * @author Tezla
 */
public class ItemModel {
    //h2
    private String title;
    //h3
    private String author;
    //files URL, any href inside table
    private LinkedList<ItemFile> files;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the files
     */
    public LinkedList<ItemFile> getFiles() {
        return files;
    }

    /**
     * @param files the files to set
     */
    void setFiles(LinkedList<ItemFile> files) {
        this.files = files;
    }
    
    @Override
    public String toString(){
        String out = this.title + "(" + this.author + ")" + "\n"
                + this.files;
        return out;
    }
}
