/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.item;

import java.util.LinkedList;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
/**
 * Convert HTML into ItemModel with ItemFiles inside.
 * This class assumes that the HTML had a .bookinfo class that contains h2, h3, and lis.
 * 
 * @author Tezla
 */
public class ItemModeler {
    public static ItemModel modelize(String htmlText, String originalURL){
        ItemModel out = new ItemModel();
        Document document = Parser.parse(htmlText, originalURL);
        Element body = document.body();
        
        //multistage safety net
        if(body != null){
            Element bookinfo = body.getElementsByClass("bookinfo").first();
            if(bookinfo != null){
                Element title = bookinfo.getElementsByTag("h2").first();
                Element author = bookinfo.getElementsByTag("h3").first();
                Elements file = bookinfo.getElementsByTag("li").select("a");

                if((title != null) && (author != null)){
                    out.setTitle(title.text().toLowerCase());
                    out.setAuthor(author.text().toLowerCase());

                    LinkedList<ItemFile> lf = new LinkedList<>();
                    for(Element f: file){
                        ItemFile itf = new ItemFile(f.attr("title"), f.attr("href"));
                        lf.add(itf);
                    }
                    out.setFiles(lf);

                    return out;
                }
                
            }
        }
        
        return null;
    }
}
