/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.parsers.item;

/**
 *
 * @author Tezla
 */
public class ItemFile {
    private String filename;
    private String url;
    
    public ItemFile(String filename, String url){
        this.filename = filename;
        this.url = url;
    }
    
    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    void setUrl(String url) {
        this.url = url;
    }
    
    @Override
    public String toString(){
        return this.filename + "[" + this.url + "]";
    }
}
