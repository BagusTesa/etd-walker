/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker;

import java.io.File;
import java.io.Reader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import etdwalker.parsers.searchresult.SearchResultModeler;
import etdwalker.parsers.searchresult.SearchResultModel;
import etdwalker.parsers.searchresult.SearchItem;
import etdwalker.parsers.item.ItemModeler;
import etdwalker.parsers.item.ItemModel;
import etdwalker.parsers.item.ItemFile;
import etdwalker.netjob.Fetcher;
/**
 *
 * @author Tezla
 */
public class Scheduler {
    /*
    *  http://etd.repository.ugm.ac.id/index.php?mod=
    *          opac
    *          &sub=Opac                //?
    *          &act=view                //?
    *          &typ=html                //?
    *          &opac_page=              //page number* - crucial, toy with this one
    *          &cari=                   //possible duplicate, but it stays empty all the time
    *          &first=1                 //first number in search?
    *          &op=opac                 //?
    *          &obyek_id=               //unset=semua, 1=blm, 2=TA, 3=Skripsi, 4=Thesis, 5=Dissertation, 6=Specialist
    *          &srcjudul=               //title
    *          &lokasi=                 //location (library), 1=Perpustakaan UGM
    *          &link=1                  //?
    *          &jenis_id=               //possible duplicate of obyek_id
    *          &cari=                   //search, full text
    */
    private String baseURL = "http://etd.repository.ugm.ac.id/";
    private String currentURL;
    private SearchResultModel currentSearch;
    
    private int total = 0;
    private int hit = 0;
    private int miss = 0;
    private int pduplicate = 0;
    
    public void readCSVandDump(String csvPath, String dumpPath){
        try {
            Reader in = new FileReader(csvPath);
            Iterable<CSVRecord> lines = CSVFormat.newFormat(';').parse(in);
            for(CSVRecord rec: lines){
                //temporarily store records
                String author = rec.get(0);
                String title = rec.get(1).replaceAll("\"|\\\\", ""); //manual cleanup, for some reason escaping '\' is not an option
                    String title_[] = title.split(";");
                String type = rec.get(2);
                String city = rec.get(3);
                String year = rec.get(4);
                String publisher = rec.get(5);
                
                System.out.println("[" + total + "] " + title_[0]);
                
                this.currentSearch = SearchResultModeler.modelize(Fetcher.fetchPagePost("http://etd.repository.ugm.ac.id/index.php?mod=opac&sub=Opac&act=view&typ=html&self=1&op=opac", title_[0]), baseURL);
                if(this.currentSearch.getItemMax() == 0){
                    this.miss++;
                }
                else{
                    this.hit++;
                    
                    //DEV NOTES: still assumes single page, did not consider next page (yet)
                    
                    for(SearchItem it: this.currentSearch.getItems()){
                        
                        System.out.println(it.getUrl());
                        
                        ItemModel srItem = ItemModeler.modelize(Fetcher.fetchPageByURL(this.baseURL + it.getUrl()), this.baseURL);
                        this.processSearchItem(srItem, dumpPath);
                    }
                }
                
                total++;
            }
            System.out.println("HIT: " + this.hit);
            System.out.println("DUP: " + this.pduplicate);
            System.out.println("MIS: " + this.miss);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void processSearchItem(ItemModel item, String dumpPath) throws IOException{
        if((item != null) && (dumpPath.length() > 0)){
            
            //truncates title length, avoid messing with filesystem
            String storageName = item.getTitle();
            if(storageName.length() > 120){
                storageName = storageName.substring(0, 120).trim();
            }
            
            if((!item.getFiles().isEmpty()) && ((new File(dumpPath + "\\" + storageName)).mkdir())){
                for(ItemFile f: item.getFiles()){
                    this.processItemFile(f, dumpPath + "\\" + storageName);
                }
            }
            else{
                //possible duplicate
                this.pduplicate++;
            }
        }
    }
    
    private void processItemFile(ItemFile file, String dumpPath) throws IOException{
        if((file != null) && (dumpPath.length() > 0)){
            InputStream source = Fetcher.fetchFile(file.getUrl());
            String path = dumpPath + "\\" + file.getFilename();
            FileOutputStream wr = new FileOutputStream(path);
            
            //known bug, if the file url should be escaped and if it had space (%20), it should be replaced by _ (1 hit for this case)
            System.out.println("\t" + path);
            
            byte buff[] = new byte[512];
            int len = -1;
            do{
                len = source.read(buff);
                wr.write(buff);
            }while(len != -1);
            
            wr.close();
            source.close();
        }
    }
    
}
