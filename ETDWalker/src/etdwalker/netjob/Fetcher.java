/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etdwalker.netjob;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Form;
/**
 *
 * @author Tezla
 */
public class Fetcher {
    private static String userAgent = "Mozilla/5.0 (compatible; Skripsi2016/2.1; +http://etd.repository.ugm.ac.id/index.php?mod=opac&sub=Opac&act=view&typ=html&self=1&op=opac)";
    
    private Fetcher(){
        //do nothing, stubs, prevent it being 'instantiated'
    }
    
    public static String getUserAgent(){
        return userAgent;
    }
    
    public static void setUserAgent(String user_Agent){
        userAgent = user_Agent;
    }
    
    private static Request applyConfig(Request req){
        if(userAgent.length() > 0){
            req.userAgent(userAgent);
        }
        return req;
    }
    
    /**
     * For exploring the other page of the search result. 
     * 
     * @param url
     * @return
     * @throws IOException
     */
    public static String fetchPageByURL(String url) throws IOException{
        return applyConfig(Request.Get(url)).execute().returnContent().asString();
    }
    
    /**
     * First time search launcher post method.
     * 
     * @param url
     * @param searchstring
     * @return
     * @throws IOException
     */
    public static String fetchPagePost(String url, String searchstring) throws IOException {
        return applyConfig(Request.Post(url).bodyForm(Form.form().add("searchstring", searchstring).build())).execute().returnContent().asString();
    }
    
    public static InputStream fetchFile(String url) throws IOException{
        return applyConfig(Request.Get(url)).execute().returnContent().asStream();
    }
    
}
